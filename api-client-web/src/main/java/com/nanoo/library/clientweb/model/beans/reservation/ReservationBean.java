package com.nanoo.library.clientweb.model.beans.reservation;

import com.nanoo.library.clientweb.model.beans.book.BookLoanBean;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author nanoo - created : 11/04/2020 - 14:20
 */
@Getter @Setter
@NoArgsConstructor
@ToString
public class ReservationBean {

    private Integer id;
    private BookLoanBean book;
    private Date reservationDate;
    private Date notificationDate;
    private int placeInQueue;
    private Date closestReturnDate;

}
