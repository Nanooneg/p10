package com.nanoo.library.clientweb.web.controller;

import com.nanoo.library.clientweb.model.beans.loan.LoanBean;
import com.nanoo.library.clientweb.model.beans.reservation.ReservationBean;
import com.nanoo.library.clientweb.model.beans.user.AccountBean;
import com.nanoo.library.clientweb.web.proxy.FeignProxy;
import com.nanoo.library.commonpackage.security.CommonSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author nanoo
 * @create 11/12/2019 - 13:11
 */
@Controller
@RequestMapping("/utilisateur")
public class UserController {
    
    private static final String LIBRARY_ATT = "libraries";
    private static final String ACCOUNT_ATT = "account";
    private static final String LOAN_LIST_ATT = "loans";
    private static final String RESERVATION_LIST_ATT = "reservations";

    private static final String USER_HOME_LOAN_VIEW = "userHomeLoanList";
    private static final String USER_HOME_RESERVATION_VIEW = "userHomeReservationList";
    private static final String REDIRECT_LOGIN_VIEW = "redirect:/login";
    private static final String REDIRECT_USER_HOME_LOAN_VIEW = "redirect:/utilisateur/home/selection";
    private static final String REDIRECT_USER_HOME_RESERVATION_VIEW = "redirect:/utilisateur/home/reservation";

    private final FeignProxy proxy;
    
    @Autowired
    public UserController(FeignProxy proxy) {
        this.proxy = proxy;
    }
    
    @GetMapping({"/home","/home/{loanProperty}"})
    public String displayUserDashBoard(Model model, @PathVariable(name = "loanProperty", required = false) String loanProperty,
                                       @CookieValue(value = CommonSecurityConfig.HEADER,required = false) String accessToken){
        
        if (accessToken == null) return REDIRECT_LOGIN_VIEW;
        
        if (loanProperty == null) loanProperty = "all";
        model.addAttribute(LIBRARY_ATT,proxy.listAllLibrary());
        
        AccountBean accountInfo = proxy.getAccountInfo(accessToken);
        List<LoanBean> userLoans = proxy.getUserLoanList(accessToken,accountInfo.getId(),loanProperty);

        model.addAttribute(ACCOUNT_ATT,accountInfo);
        model.addAttribute(LOAN_LIST_ATT,userLoans);
    
        return USER_HOME_LOAN_VIEW;
        
    }
    
    @GetMapping("/etendre/{loanId}")
    public String extendLoanExpectedReturnDate(@CookieValue(value = CommonSecurityConfig.HEADER,required = false) String accessToken,
                                               @PathVariable("loanId") int loanId){
        
        proxy.extendLoanExpectedReturnDate(accessToken,loanId);
        
        return REDIRECT_USER_HOME_LOAN_VIEW;
    }

    @GetMapping("/home/reservation")
    public String displayUserReservation(Model model, @CookieValue(value = CommonSecurityConfig.HEADER,required = false) String accessToken){

        if (accessToken == null) return REDIRECT_LOGIN_VIEW;

        model.addAttribute(LIBRARY_ATT,proxy.listAllLibrary());

        AccountBean accountInfo = proxy.getAccountInfo(accessToken);
        List<ReservationBean> userReservations = proxy.getUserReservationList(accessToken,accountInfo.getId());

        model.addAttribute(ACCOUNT_ATT,accountInfo);
        model.addAttribute(RESERVATION_LIST_ATT,userReservations);

        return USER_HOME_RESERVATION_VIEW;
    }

    @GetMapping("/home/remove/{reservationId}")
    public String cancelReservation(@PathVariable int reservationId, @CookieValue(value = CommonSecurityConfig.HEADER,required = false) String accessToken){
        proxy.removeReservation(accessToken,reservationId);
        return REDIRECT_USER_HOME_RESERVATION_VIEW;
    }
    
}
