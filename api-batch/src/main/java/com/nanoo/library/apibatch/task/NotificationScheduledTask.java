package com.nanoo.library.apibatch.task;

import com.nanoo.library.apibatch.authentication.CredentialBatch;
import com.nanoo.library.apibatch.job.CustomerMailing;
import com.nanoo.library.apibatch.job.ReservationOutdatedDeletion;
import com.nanoo.library.apibatch.web.proxy.FeignProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author nanoo
 * @created 06/04/2020 - 13:53
 */
@Service
@EnableScheduling
@EnableAsync
public class NotificationScheduledTask {

    private final FeignProxy proxy;
    private final ReservationOutdatedDeletion reservationOutdatedDeletion;
    private final CustomerMailing customerMailing;

    @Autowired
    public NotificationScheduledTask(FeignProxy proxy, ReservationOutdatedDeletion reservationOutdatedDeletion, CustomerMailing customerMailing) {
        this.proxy = proxy;
        this.reservationOutdatedDeletion = reservationOutdatedDeletion;
        this.customerMailing = customerMailing;
    }

    /**
     * (cron="*\/5 * * * * ?") prod context : run task every 5 minutes (without "\" before 5)
     * (cron="* * * * * ?") test context : run task every seconds
     *
     * Describe here TODO
     *
     */
    @Scheduled(cron="0 3 * * * ?")
    public void runScheduledTask(){

        String accessToken = proxy.doLogin(new CredentialBatch());

        Map<String,String> nextCustomersEmailAndBookTitle = reservationOutdatedDeletion.removeOldReservationAndGetNext(accessToken);
        customerMailing.notifyCustomer(nextCustomersEmailAndBookTitle);

    }
}
