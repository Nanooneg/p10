package com.nanoo.library.apibatch.task;

import com.nanoo.library.apibatch.authentication.CredentialBatch;
import com.nanoo.library.apibatch.job.CustomerMailing;
import com.nanoo.library.apibatch.job.LoanStatusUpdate;
import com.nanoo.library.apibatch.web.proxy.FeignProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author nanoo
 * @create 05/02/2020 - 12:09
 */
@Service
@EnableScheduling
@EnableAsync
public class RevivalScheduledTask {
    
    private LoanStatusUpdate loanStatusUpdate;
    private CustomerMailing customerMailing;
    private FeignProxy proxy;
    
    @Autowired
    public RevivalScheduledTask(LoanStatusUpdate loanStatusUpdate, CustomerMailing customerMailing, FeignProxy proxy) {
        this.loanStatusUpdate = loanStatusUpdate;
        this.customerMailing = customerMailing;
        this.proxy = proxy;
    }
    
    
    /**
     * (cron="0 3 * * * ?") prod context : run task every days at 03:00 am
     * (cron="* * * * * ?") test context : run task every seconds
     *
     * First, get a security token with technical account and call for 2 jobs :
     * - update loan status
     * - revive customers with outdated loans
     *
     */
    @Scheduled(cron="0 3 * * * ?")
    public void runScheduledTask(){
    
        String accessToken = proxy.doLogin(new CredentialBatch());
    
        loanStatusUpdate.updateStatus(accessToken);
        customerMailing.reviveCustomer(accessToken);
        
    }
}
