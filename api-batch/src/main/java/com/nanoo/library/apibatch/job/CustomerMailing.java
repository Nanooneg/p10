package com.nanoo.library.apibatch.job;

import com.nanoo.library.apibatch.web.proxy.FeignProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * @author nanoo
 * @create 28/01/2020 - 11:08
 */
@Service
public class CustomerMailing {

    private Logger logger = LoggerFactory.getLogger(CustomerMailing.class);

    public static final String REVIVAL_MAIL_SUBJECT = "Vous êtes en retard pour le retour de votre livre";
    public static final String NOTIFICATION_MAIL_SUBJECT = "Votre livre est disponible";

    private final FeignProxy proxy;
    private final JavaMailSender emailSender;
    private final SimpleMailMessage templateSimpleMessage;

    @Autowired
    public CustomerMailing(FeignProxy proxy, JavaMailSender emailSender, SimpleMailMessage templateSimpleMessage) {
        this.proxy = proxy;
        this.emailSender = emailSender;
        this.templateSimpleMessage = templateSimpleMessage;
    }

    
    /**
     * This method call ms-loan to get an email list back and send a message to each one
     *
     * @param accessToken security token
     */
    public void reviveCustomer (String accessToken){
    
        Map<String,Date> customersEmailAndExpectedReturnDate = proxy.getOutdatedLoanEmails(accessToken);
        
        for (Map.Entry<String,Date> entry : customersEmailAndExpectedReturnDate.entrySet()){
            sendSimpleMessage(entry.getKey(),REVIVAL_MAIL_SUBJECT,getExpectedReturnDateFormatted(entry.getValue()),null);
        }
        
    }

    /**
     * TODO
     *
     */
    public void notifyCustomer (Map<String,String> customersEmailAndBookTitle){

        for (Map.Entry<String,String> entry : customersEmailAndBookTitle.entrySet()){
            sendSimpleMessage(entry.getKey(),NOTIFICATION_MAIL_SUBJECT,getAvailabilityForReservation(),entry.getValue());
        }

    }

    /**
     * This method format the expected return date
     *
     * @param date a date
     * @return a formatted date
     */
    private String getExpectedReturnDateFormatted (Date date){
        String pattern = "dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        
        return simpleDateFormat.format(date);
        
    }

    /**
     * This method format the expected return date
     *
     * @return a formatted date
     */
    private String getAvailabilityForReservation (){
        String pattern = "dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_WEEK,2);

        return simpleDateFormat.format(calendar.getTime());

    }

    /**************************/
    /**** Mailing process *****/
    /**************************/

    /**
     * This method build and send an email with a template
     *
     * @param to mail address of recipient
     * @param arg1 argument to insert in email body
     * @param arg2 argument to insert in email body
     */
    public void sendSimpleMessage(String to, String subject, String arg1, String arg2  ) {

        logger.info("*** Mail sending to : {} ***", to);
        SimpleMailMessage message = new SimpleMailMessage(templateSimpleMessage);

        String text = String.format(Objects.requireNonNull(message.getText()),arg1,arg2);

        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);

        emailSender.send(message);

    }

}
