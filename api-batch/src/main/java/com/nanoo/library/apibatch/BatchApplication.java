package com.nanoo.library.apibatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;

@SpringBootApplication
@EnableFeignClients("com.nanoo.library.apibatch")
@EnableEurekaClient
public class BatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchApplication.class, args);
	}

	// TODO try to use 2 different template or use common template to both task.

	/**
	 * Notification mail template
	 */
	@Bean
	public SimpleMailMessage templateNotificationMessage() {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setText("Bonjour," +
				"\n\nVotre réservation est disponible. Vous avez jusqu'au %s pour la retirer." +
				"\nPassée cette date, vous serez retiré de la liste d'attente pour le livre : %s." +
				"\n\n\t\t\t\tBibliothèque de Bordeaux - Ceci est un envoi automatique, merci de ne pas y répondre.");
		return message;
	}

	/**
	 * Revival mail template
	 */
    /*@Bean
    public SimpleMailMessage templateRevivalMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText("Bonjour," +
                "\n\nLa date de retour pour votre livre était le : %s" +
                "\nVous avez la possibilité, si vous ne l'avez pas encore fait, de prolonger la durée de votre emprunt de 4 semaines." +
                "\nSinon, pensez à ramener le livre à votre bibliothèque au plus vite pour éviter une pénalité" +
                "\n\n\t\t\t\tBibliothèque de Bordeaux - Ceci est un envoi automatique, merci de ne pas y répondre.");
        return message;
    }*/
}
