package com.nanoo.library.loan.service.impl;

import com.nanoo.library.commonpackage.exception.FunctionalException;
import com.nanoo.library.commonpackage.model.Status;
import com.nanoo.library.loan.database.BookRepository;
import com.nanoo.library.loan.database.ClientRepository;
import com.nanoo.library.loan.database.LoanRepository;
import com.nanoo.library.loan.database.ReservationRepository;
import com.nanoo.library.loan.model.dto.ClientDto;
import com.nanoo.library.loan.model.dto.ReservationDto;
import com.nanoo.library.loan.model.entities.Book;
import com.nanoo.library.loan.model.entities.Client;
import com.nanoo.library.loan.model.entities.Loan;
import com.nanoo.library.loan.model.entities.Reservation;
import com.nanoo.library.loan.model.mapper.BookMapper;
import com.nanoo.library.loan.model.mapper.ClientMapper;
import com.nanoo.library.loan.model.mapper.ReservationMapper;
import com.nanoo.library.loan.service.contract.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author nanoo
 * @created 05/04/2020 - 14:19
 */
@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    private static final int RESERVATION_DAYS_DURATION = 2;

    private final ReservationRepository reservationRepository;
    private final ClientRepository clientRepository;
    private final ReservationMapper reservationMapper;
    private final ClientMapper clientMapper;
    private final LoanRepository loanRepository;
    private final BookMapper bookMapper;
    private final BookRepository bookRepository;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository, ClientRepository clientRepository, ReservationMapper reservationMapper, ClientMapper clientMapper, LoanRepository loanRepository, BookMapper bookMapper, BookRepository bookRepository) {
        this.reservationRepository = reservationRepository;
        this.clientRepository = clientRepository;
        this.reservationMapper = reservationMapper;
        this.clientMapper = clientMapper;
        this.loanRepository = loanRepository;
        this.bookMapper = bookMapper;
        this.bookRepository = bookRepository;
    }


    @Override
    public List<ReservationDto> getReservationList() {
        return reservationMapper.fromReservationsToDtos(reservationRepository.findAll());
    }

    @Override
    public List<ReservationDto> getUserReservationList(int userId) {
        List<Reservation> reservations = clientRepository.findAllReservationFromClient(userId);
        List<ReservationDto> reservationDtos = new ArrayList<>();
        for (Reservation reservation : reservations){
            ReservationDto reservationDto = reservationMapper.fromReservationToDto(reservation);
            reservationDto.setPlaceInQueue(findPlaceInQueue(reservation));
            reservationDto.setClosestReturnDate(findClosestReturnDate(reservation));
            reservationDtos.add(reservationDto);
        }
        return reservationDtos;
    }

    @Override
    public ReservationDto createReservation(int bookId, ClientDto clientDto) {
        ReservationDto reservationDto = new ReservationDto();
        Optional<Book> book = bookRepository.findById(bookId);
        if (book.isPresent()){
            reservationDto.setBook(bookMapper.fromBookToDto(book.get()));
            reservationDto.setClient(clientDto);
            reservationDto.setReservationDate(new Date());
            try{
                checkReservationValidity(reservationDto);
            }catch (FunctionalException e){
                return null;
            }
            Reservation reservation = reservationRepository.save(reservationMapper.fromDtoToReservation(reservationDto));
            return reservationMapper.fromReservationToDto(reservation);
        }else{
            return null;
        }
    }

    @Override
    public void deleteReservation(int reservationId) {
        Optional<Reservation> reservation = reservationRepository.findById(reservationId);
        reservation.ifPresent(reservationRepository::delete);
    }

    @Override
    public ClientDto getFirstReservation(int bookId) {
        Page<Client> page = reservationRepository.findFirstClientFromBookId(bookId, PageRequest.of(0, 1));
        if (page.hasContent()) {
            Client client = page.getContent().get(0);
            return clientMapper.fromClientToDto(client);
        }
        return null;
    }

    @Override
    public Map<String, String> deleteOutdatedReservationAndGetNext() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_WEEK, -RESERVATION_DAYS_DURATION);
        List<Reservation> outdatedReservation = reservationRepository.findAllOutdated(calendar.getTime());
        reservationRepository.deleteAll(outdatedReservation);
        return getNextEmailAccountToNotify(outdatedReservation);
    }

    @Override
    public boolean isPriority(int clientId, int bookId){
        ClientDto nextClient = this.getFirstReservation(bookId);
        if (nextClient != null) {
            return clientId == nextClient.getId();
        }else {
            return true;
        }
    }

    public void checkReservationValidity(ReservationDto reservationDto) throws FunctionalException {
        int reservations = reservationRepository.getNumberOfReservation(reservationDto.getBook().getId());
        if (reservations >= reservationDto.getBook().getCopies() * 2){
            throw new FunctionalException("Le nombre maximal de réservations pour ce livre a été atteind.");
        }
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Loan> loans = loanRepository.findAllLoansNotFinishByEmail(email);
        if (!loans.isEmpty()){
            for (Loan loan : loans) {
                if (loan.getCopyBook().getBook().getId().equals(reservationDto.getBook().getId()) && loan.getStatus() != Status.FINISH){
                    throw new FunctionalException("Vous avez déjà un emprunt en cours pour ce livre.");
                }
            }
        }
    }

    private Map<String, String> getNextEmailAccountToNotify(List<Reservation> reservationList) {
        Map<String, String> emailsAndTitle = new HashMap<>();
        for (Reservation reservation : reservationList) {
            ClientDto nextClient = getFirstReservation(reservation.getBook().getId());
            if ( nextClient != null) {
                emailsAndTitle.put(nextClient.getEmail(), reservation.getBook().getTitle());
                reservationRepository.updateReservation(nextClient.getId(),reservation.getBook().getId());
            }
        }
        return emailsAndTitle;
    }

    private int findPlaceInQueue(Reservation reservation) {
        List<Reservation> reservationList = reservationRepository.findAllByBookId(reservation.getBook().getId());
        reservationList.sort(Comparator.comparing(Reservation::getReservationDate));
        return reservationList.indexOf(reservation);
    }

    public Date findClosestReturnDate(Reservation reservation) {
        List<Loan> loans = loanRepository.findAllLoanByBookId(reservation.getBook().getId());
        loans.sort(Comparator.comparing(Loan::getExpectedReturnDate).reversed());
        if (!loans.isEmpty())
            return loans.get(0).getExpectedReturnDate();
        else
            return null;
    }

}
