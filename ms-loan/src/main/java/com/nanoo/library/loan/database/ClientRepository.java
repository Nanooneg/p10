package com.nanoo.library.loan.database;

import com.nanoo.library.loan.model.entities.Client;
import com.nanoo.library.loan.model.entities.Loan;
import com.nanoo.library.loan.model.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author nanoo
 * @create 23/01/2020 - 13:03
 */
@Repository
public interface ClientRepository extends JpaRepository<Client,Integer> {

    @Query(value = "SELECT c.reservations FROM Client c " +
                   "WHERE c.id = :clientId")
    List<Reservation> findAllReservationFromClient (@Param("clientId") int clientId);

}
