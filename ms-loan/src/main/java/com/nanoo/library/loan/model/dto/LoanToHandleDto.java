package com.nanoo.library.loan.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author nanoo - created : 17/04/2020 - 18:57
 */
@Getter @Setter
@NoArgsConstructor
public class LoanToHandleDto {

    private static final long serialVersionUID = 1L;

    private ClientDto client;
    private CopyBookDto copyBook;

}
